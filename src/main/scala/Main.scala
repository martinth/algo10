package object cnf {
	/* we encode our configuration as an indexed sequence of Boolean. Each variables
	 * x_n value is placed in bucket n */
	type Config = IndexedSeq[Boolean]
}

import cnf.{Config}
import scala.collection.parallel.immutable.ParVector
import scala.util.Random
import scala.collection.immutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.actors.Futures._
import scala.concurrent.duration._
import scala.math.{abs, ceil, floor, pow}
import util.control.Breaks._
import scala.actors.remote.RemoteActor._
import scala.actors.Actor
import org.rogach.scallop._
import PartialFunction.cond

/** a variable **/
class Var(val idx: Int, negated: Boolean = false) {
  def eval(config: Config) = if (negated) !config(idx) else config(idx)
  override def toString = if (negated) s"~x$idx" else s"x$idx"
}

/** a clause **/
class Clause(val vars: Seq[Var]) {
  def eval(config: Config) = vars exists (_.eval(config))
  def length = vars.length
  override def toString = vars.mkString(" ∨ ")
}

/** the whole formula **/
class Formula(val k: Int, val n: Int, clauses: Seq[Clause]) {
  def unsatisfied(config: Config) = clauses filter (_.eval(config) == false) 
  override def toString = clauses map (c => "(" + c.toString + ")") mkString " ∧ "
}

/** Helper to create Formula based on two random distribtuions */
class DistribtionBuilder(id: Int, k: Int) {
  
  val rnd = new Random(System.currentTimeMillis())
  
  /** create a Formula with numClauses Clauses out of numVars different Variables for **/
  def build(numClauses: Int, numVars: Int) = {
    id match {
      case 1 => new Formula(k, numVars, List.fill(numClauses)(uniformClause(numVars)))
      case 2 => new Formula(k, numVars, List.fill(numClauses)(differentVarClause(numVars)))
      case _ => throw new IllegalArgumentException()
    }
  }
  
  def uniformClause(n: Int) = {
    new Clause(Array.fill(k) {
      new Var(rnd.nextInt(n), rnd.nextBoolean)
    })
  }
  
  def differentVarClause(n: Int) = {
    require(n >= k, "n must be at least as large as k")
    
    // from the shuffled range from 0 to n create k new Variables with random negation
    new Clause(rnd.shuffle((0 until n).toList) take(k) map (new Var(_, rnd.nextBoolean)))
  }
}

/** Solves k-CNF Formula **/
class Solver {
  val rnd = new Random(System.currentTimeMillis())
  
  /** finds a solution for Formula f with a minimum probability of e^(-g) **/
  def findSolution(f: Formula, g: Int = 10):Option[Config] = {
    
    // calc the number of restarts required to reach minimum probability
    val restarts = ceil(g / (2f/3f * pow(0.5 * (f.k.toFloat/(f.k-1)), f.n))).toInt
    
    // at most as many restarts as calculated with early return
    for (i <- 0 until restarts) {
      val res = iteration(f)
      if (res.isDefined) return res
    }
    return None
  }
  
  /** one iteration of the random k-SAT algorithm **/
  private def iteration(f: Formula):Option[Config] = {
    // random start configuration 
    val config = Array.fill(f.n)(rnd.nextBoolean)
    
    for(i <- 0 until 3 * f.n) {
      // get all unsatisfied clauses from formula
      val unsatClauses = f.unsatisfied(config)
      
      if(unsatClauses.length == 0) {
        // if there are none, we have found a solution and return a result
        return Some(config)
      } else {
        // otherwise we toggle on random variable of one of the unsatisfied clauses
        val vars = unsatClauses(0).vars
        val rndVarIdx = vars(rnd.nextInt(vars.length)).idx
        config(rndVarIdx) = !config(rndVarIdx)
      }
    }
    return None
  }
}

/** wrapper for the calculation functions **/
object Calc {
  
  val rounds = 100
  
  def sigma(distId: Int)(k: Int)(numVars: Int, numClauses: Int) = {
    val builder = new DistribtionBuilder(id = distId, k)
    val solver = new Solver()
    
    
    val results = (1 to rounds).toList.par.map(x => {
      val start = System.nanoTime()
      val solution  = solver.findSolution(builder.build(numClauses, numVars))
      val elapsed = System.nanoTime() - start

      solution match {
        case Some(solution) => (true, elapsed)
        case None           => (false, elapsed)
      }
    })
    
    val notSolveablePercentage = results.count(_._1 == false) / rounds.toDouble
    val avgTime = Duration( (results map(_._2) sum) / rounds, NANOSECONDS )
    
    (notSolveablePercentage, avgTime)
  }
  
  
  def m(distId: Int)(k: Int)(n: Int, mStart: Int = 1):(Int, Duration) = { 
    
    val partialSigma = sigma(distId)(k)_
    
    var last = (0.0, 0 millis)
    
    for(m <- Stream.from(mStart)) {
      val (last_σ, last_runtime) = last
      
      val (σ, runtime) = partialSigma(n, m)
      
      if (σ >= 0.5) {
        if(abs(0.5 - σ) < (abs(0.5 - last_σ))) {
          return (m, runtime)
        } else {
          return (m-1, last_runtime)
        }
      } else {
        last = (σ, runtime)
      }
    }
    return (0, 0 millis)
  }
}

object Main {
  def main(args: Array[String]) {
    /* we parse commandline parameter first, so we have Conf in scope
	 * (no need to pass it around) */
    object Conf extends ScallopConf(args) {
      val k = opt[Int](default=Some(3), descr="length of clauses")
      val m = opt[Int](default=Some(1), descr="start value for number of clauses")
      val n = opt[Int](default=Some(10), descr="start value of numer" )
      val limit = opt[Int]("timelimit", descr="manually override timelimit (in ms) Default: k seconds")
      val skipA = opt[Boolean]("skip-a", descr="skip part a)")
      val dist = opt[Int]("distribution", descr="which distribution (default: all)")
    }

    /** calculate n_max and sigma for distribution distId **/
    def doForDistribution(distId: Int) {
      var lastM = Conf.m()
      var nMax = Conf.n()

      // maybe the user requested to skip part a
      if (!Conf.skipA()) {
        val limit = if (Conf.limit.isDefined) {
          Duration(Conf.limit(), MILLISECONDS)
        } else {
          Duration(Conf.k(), SECONDS)
        }
       
        val mPartial = Calc.m(distId)(Conf.k())_

        println("dist, n, m, avg time, step time")
        breakable {
          for (n <- Conf.n() to Int.MaxValue) {
            val start = System.nanoTime()
            val (m, avgRuntime) = mPartial(n, lastM)
            val elapsed = Duration(System.nanoTime() - start, NANOSECONDS)
            // if we reach our time limit we break, otherwise print and continue
            if (avgRuntime <= limit) {
              lastM = m
              println(s"$distId, $n, $lastM, ${avgRuntime.toMillis} ms, ${elapsed.toSeconds} s")
            } else {
              nMax = n
              break;
            }

          }
        }

      }

      // part b)
      val sigmaPartial = Calc.sigma(distId)(Conf.k())_

      println("dist, max_n, m, sigma")
      val tenPercent = lastM * 0.1
      for (m <- floor(lastM - tenPercent).toInt to ceil(lastM + tenPercent).toInt) {
        val (σ, runtime) = sigmaPartial(nMax, m)
        println(s"$distId, $nMax, $m, $σ")
      }

    }

    // if the user specified one distribution, we use this, otherwise both
    if (Conf.dist.isDefined) {
      doForDistribution(Conf.dist());
    } else {
      doForDistribution(1);
      doForDistribution(2);
    }

  }
}

