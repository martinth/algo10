import AssemblyKeys._ // put this at the top of the file

name := "algo10"

version := "1.0"

scalaVersion := "2.10.0"

libraryDependencies += "org.scala-lang" % "scala-actors" % "2.10.0"

libraryDependencies += "org.rogach" %% "scallop" % "0.7.0"

assemblySettings
