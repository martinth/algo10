# -*- coding: utf-8 -*-
import sys
import numpy as np
import matplotlib.pyplot as plt
from os.path import splitext

def graph_data(infile):
    filename, ext = splitext(infile)
    data = np.genfromtxt(infile, delimiter=',', comments='#')
    dist = int(data[0,0])
    n_max = int(data[0,1])

    ax = plt.subplot(111)
    ax.plot(data[:,2], data[:,3], 'bo-')

    ax.xaxis.set_label_text('m')
    ax.xaxis.grid(color='gray', linestyle='dashed')

    ax.yaxis.set_label_text(u'$σ_{k}^{dist}({nmax}, m)$'.format(k=3, dist=dist, nmax=n_max))
    ax.yaxis.grid(color='gray', linestyle='dashed')

    plt.savefig(filename + '.pdf')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "provide input file as parameter"
        sys.exit(1)
    graph_data(sys.argv[1])

